package model_data_structures;

import java.util.Iterator;

public class Stack<T extends Comparable<T>> implements IStack<T> {

	//-----------------------------------------------------------
	// Node class
	//-----------------------------------------------------------

	private class Node {

		T element;

		Node next;

		public Node(T element) {
			this.element = element;
			this.next = null;
		}

		public T getElement() {
			return element;
		}

		public Node getNext() {
			return next;
		}

		public void setNext(Node next) {
			this.next = next;
		}
	}

	private Node firstNode;

	private int listSize;

	public Stack() {
		firstNode = null;
		listSize = 0;
	}

	public Iterator<T> iterator() {
		return new Iterator<T>() {
			Node act = null;

			@Override
			public boolean hasNext() {
				if (listSize == 0) {
					return false;
				}

				if (act == null) {
					return true;
				}

				return act.getNext() != null;
			}

			@Override
			public T next() {
				if (act == null) {
					act = firstNode;
				} else {
					act = act.getNext();
				}

				return act.getElement();
			}
		};
	}

	public boolean isEmpty() {
		return firstNode == null;
	}

	public int size() {
		return listSize;
	}

	public void push(T t) {
		Node oldFirst = firstNode;
		firstNode = new Node(t);

		firstNode.setNext(oldFirst);
		listSize ++;
	}

	public T pop() {
		T element = null;
		
		if(!isEmpty()) {
			element = firstNode.getElement();
			firstNode = firstNode.getNext();
			
			listSize --;
		} else {
			return element;
		}

		return element;
	}

}

