package model_data_structures;

public interface IStack<T> extends Iterable<T> {

	public boolean isEmpty();

	public int size();

	public void push(T t);

	public T pop();	
}
