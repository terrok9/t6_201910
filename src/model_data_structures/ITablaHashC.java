package model_data_structures;

public interface ITablaHashC <K extends Comparable<K>, V> {

	void put(K key, V value);
	
    V get(K key);
	
	V delete(K key);
}
