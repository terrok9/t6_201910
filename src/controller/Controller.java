package controller;

import java.io.FileReader;
import java.util.Iterator;
import java.util.Scanner;



import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import consola.MovingViolationsManagerView;
import contenedora.VOMovingViolations;
import model_data_structures.TablaHashLinearProbing;
import model_data_structures.TablaHashSeparateChaining;

public class Controller {

private String[] semestre;

private TablaHashLinearProbing lp;


private TablaHashSeparateChaining sc;

private MovingViolationsManagerView view;

public Controller(){
	view = new MovingViolationsManagerView();
	
	sc = new TablaHashSeparateChaining<>(10000);
	lp = new TablaHashLinearProbing<>(10000);
	semestre = new String[6];
}
public String[] semestre(int pNumsemestre){
	if(pNumsemestre == 1){
		semestre[0] = "January";
		semestre[1] = "February";
		semestre[2] = "March";
		semestre[3] = "April";
		semestre[4] = "May";
		semestre[5] = "June";
	}
	else{
		semestre[0] = "July";
		semestre[1] = "August";
		semestre[2] = "September";
		semestre[3] = "October";
		semestre[4] = "November";
		semestre[5] = "December";
	}
	for(int i = 0; i < semestre.length; i++){
	System.out.println(semestre[i]);
	}
	return semestre;
}
public void run(){
	Scanner sc = new Scanner(System.in);
	boolean fin=false;
	Controller controller = new Controller();
	
	while(!fin)
	{
		view.printMenu();
		
		int option = sc.nextInt();
		
		switch(option){
		case 0:
			view.printMessage("Escriba el n�mero 1");
			int pNumeroSemestre = sc.nextInt();
			controller.readerJson(pNumeroSemestre);
			break;
		case 1:
			controller.tablareq1();
			break;
			
		case 2:
			
			controller.tablareq2();
			break;
			
		case 3:
			
			fin=true;
			sc.close();
			
			break;
		}
	}
}
public void readerJson(int pNumeroSemestre){
	semestre = semestre(pNumeroSemestre);
	JsonParser jsonparser = new JsonParser();
  try{
	  FileReader reader1 =  new FileReader("data/Moving_Violations_Issued_in_"+ semestre[0] +"_2018.json");
	  FileReader reader2 =  new FileReader("data/Moving_Violations_Issued_in_"+ semestre[1] +"_2018.json");
	  FileReader reader3 =  new FileReader("data/Moving_Violations_Issued_in_"+ semestre[2] +"_2018.json");
	  FileReader reader4 =  new FileReader("data/Moving_Violations_Issued_in_"+ semestre[3] +"_2018.json");
	  FileReader reader5 =  new FileReader("data/Moving_Violations_Issued_in_"+ semestre[4] +"_2018.json");
	  FileReader reader6 =  new FileReader("data/Moving_Violations_Issued_in_"+ semestre[5] +"_2018.json");
  Object obj1 = jsonparser.parse(reader1);
  JsonArray movingviolation1 = (JsonArray) obj1;
  System.out.println(movingviolation1.size());  
  parseJsonObject(movingviolation1);
  }
  catch(Exception e){
	  e.printStackTrace();
  }
}
public void parseJsonObject(JsonArray a){
	Iterator iter = a.iterator();
	while(iter.hasNext()){
		JsonObject jsonobject = a.getAsJsonObject();
		JsonElement objectid = jsonobject.get("OBJECTID");
		JsonElement location = jsonobject.get("LOCATION");
		JsonElement addressid = jsonobject.get("ADDRESS_ID");
		JsonElement streetsegid = jsonobject.get("STREETSEGID");
		JsonElement XCOORD = jsonobject.get("XCOORD");
		JsonElement YCOORD = jsonobject.get("YCOORD");
		JsonElement TICKETTYPE = jsonobject.get("TICKETTYPE");
		JsonElement FINEAMT = jsonobject.get("FINEAMT");
		JsonElement TOTALPAID = jsonobject.get("TOTALPAID");
		JsonElement PENALTY1 = jsonobject.get("PENALTY1");
		JsonElement PENALTY2 = jsonobject.get("PENALTY2");
		JsonElement ACCIDENTINDICATOR = jsonobject.get("ACCIDENTINDICATOR");
		JsonElement TICKETISSUEDATE = jsonobject.get("TICKETISSUEDATE");
		JsonElement VIOLATIONCODE = jsonobject.get("VIOLATIONCODE");
		JsonElement VIOLATIONDESC = jsonobject.get("VIOLATIONDESC");
	 if(addressid.getAsString().equals("null")){
		 //DO NOTHING
	 }
	 else{
		 VOMovingViolations first = new VOMovingViolations(objectid.getAsInt(), location.getAsString(), addressid.getAsInt(), streetsegid.getAsInt(), XCOORD.getAsDouble(), YCOORD.getAsDouble(), TICKETTYPE.getAsString(), FINEAMT.getAsDouble(), TOTALPAID.getAsInt(), PENALTY1.getAsString(), PENALTY2.getAsString(), ACCIDENTINDICATOR.getAsString(), TICKETISSUEDATE.getAsString(), VIOLATIONCODE.getAsString(), VIOLATIONDESC.getAsString()  );
		 sc.put(first.getAddressId(), first);
		 lp.put(first.getAddressId(), first);
	 }
		
	}
}
public TablaHashSeparateChaining tablareq2(){
	return sc;
}

public TablaHashLinearProbing tablareq1(){
	return lp;
}
}
