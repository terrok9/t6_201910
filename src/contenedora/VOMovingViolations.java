package contenedora;

public class VOMovingViolations {

	private int ObjectId;

	private String Location;

	private int AddressId;

	private int StreetSegId;

	private double xCoord;

	private double yCoord;

	private String ticketType;

	private double FineAmount;

	private int valor;

	private String penalty1;

	private String penalty2;

	private String accidente;

	private String Date;

	private String violationCode;

	private String violation;

	public VOMovingViolations(int pObjectId, String pLocation, int pAdressId, int pStreetSegId, double pXcoord, double pYcoord, String pTicketType, double pFineAmount, int pValor, String pPenalty1, String pPenalty2, String pAccidente, String pDate, String pViolationCode, String pViolation){
		ObjectId = pObjectId;
		Location = pLocation;
		AddressId = pAdressId;
		StreetSegId = pStreetSegId;
		xCoord = pXcoord;
		yCoord = pYcoord;
		ticketType = pTicketType;
		FineAmount = pFineAmount;
		valor = pValor;
		penalty1 = pPenalty1;
		penalty2 = pPenalty2;
		accidente = pAccidente;
		Date = pDate;
		violationCode = pViolationCode;
		violation = pViolation;
	}

	/**
	 * @return id - Identificador �nico de la infracci�n
	 */
	public int objectId() {
		// TODO Auto-generated method stub
		return ObjectId;
	} 


	/**
	 * @return location - Direcci�n en formato de texto.
	 */
	public String getLocation() {
		// TODO Auto-generated method stub
		return Location;
	}

	/**
	 * @return date - Fecha cuando se puso la infracci�n .
	 */
	public String getTicketIssueDate() {
		// TODO Auto-generated method stub
		return Date;
	}

	/**
	 * @return totalPaid - Cuanto dinero efectivamente pag� el que recibi� la infracci�n en USD.
	 */
	public int getTotalPaid() {
		// TODO Auto-generated method stub
		return valor;
	}

	/**
	 * @return accidentIndicator - Si hubo un accidente o no.
	 */
	public String  getAccidentIndicator() {
		// TODO Auto-generated method stub
		return accidente;
	}

	/**
	 * @return description - Descripci�n textual de la infracci�n.
	 */
	public String  getViolationDescription() {
		// TODO Auto-generated method stub
		return violation;
	}

	public int getAddressId() {
		// TODO Auto-generated method stub
		return AddressId;
	}

	public int getStreetSegId() {
		// TODO Auto-generated method stub
		return StreetSegId;
	}
	public String getPenalty1(){
		return penalty1;
	}
	public String getPenalty2(){
		return penalty2;
	}
	public String getViolationCode(){
		return violationCode;
	}
	public double getFineAmount(){
		return FineAmount;
	}

	public double getxCoord() {
		return xCoord;
	}

	public double getyCoord() {
		return yCoord;
	}

	public String getTicketType() {
		return ticketType;
	}

}

