package consola;

public class MovingViolationsManagerView 
{
	/**
	 * Constante con el nÃºmero maximo de datos maximo que se deben imprimir en consola
	 */
	public static final int N = 20;
	
	public void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller6----------------------");
		System.out.println("0. Cargar datos del primer semestre");
		System.out.println("1. TablaHashLinearProbing");
		System.out.println("2. TablaHashSeparateChaining");
		System.out.println("3. Salir");
		
	}
	
	public void printMessage(String mensaje) {
		System.out.println(mensaje);
	}
	
	
}

